//
//  PCMailSender.m
//  PhotoCollector
//
//  Created by Igor Novik on 2015-03-19.
//  Copyright (c) 2015 CodabraSoft. All rights reserved.
//

#import "PCMailSender.h"
#import "MailCore.h"

@implementation PCMailSender

+ (void)sendEmailWithSubject:(NSString *)subject message:(NSString *)message forRecipients:(NSArray *)recipients completion:(void(^)(NSError *error))completion
{
    MCOSMTPSession *smtpSession = [[MCOSMTPSession alloc] init];
    smtpSession.hostname = @"smtp.gmail.com";
    smtpSession.port = 465;
    smtpSession.username = @"photocollectormail@gmail.com";
    smtpSession.password = @"photocollector$123";
    smtpSession.authType = MCOAuthTypeSASLPlain;
    smtpSession.connectionType = MCOConnectionTypeTLS;
    
    MCOMessageBuilder *builder = [[MCOMessageBuilder alloc] init];
    MCOAddress *from = [MCOAddress addressWithDisplayName:@"Quest game photos"
                                                  mailbox:@"photocollectormail@gmail.com"];
    
    NSMutableArray *recipientsCc = [NSMutableArray arrayWithCapacity:[recipients count]];
    for (NSString *email in recipients) {
        MCOAddress *toCc = [MCOAddress addressWithDisplayName:nil
                                                      mailbox:email];
        [recipientsCc addObject:toCc];
    }
    
    MCOAddress *to = [MCOAddress addressWithDisplayName:nil
                                                mailbox:@"exclucive@yandex.ru"];
    
    [[builder header] setFrom:from];
    [[builder header] setTo:@[to]];
    [[builder header] setCc:recipientsCc];
    [[builder header] setSubject:subject];
    
    [builder setHTMLBody:message];
    NSData * rfc822Data = [builder data];
    
    MCOSMTPSendOperation *sendOperation = [smtpSession sendOperationWithData:rfc822Data];
    
    [sendOperation start:^(NSError *error) {
        completion(error);
    }];
}

@end
