//
//  PCFilesManager.h
//  PhotoCollector
//
//  Created by Igor Novik on 2015-03-13.
//  Copyright (c) 2015 CodabraSoft. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PCFilesManager : NSObject

+ (void)saveImage:(UIImage *)image withName:(NSString *)name;
+ (void)removeImageWithName:(NSString *)imageName;
+ (UIImage *)imageWithName:(NSString *)imageName;

@end
