//
//  PCFilesManager.m
//  PhotoCollector
//
//  Created by Igor Novik on 2015-03-13.
//  Copyright (c) 2015 CodabraSoft. All rights reserved.
//

#import "PCFilesManager.h"

@implementation PCFilesManager

+ (void)saveImage:(UIImage *)image withName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[name stringByAppendingPathExtension:@"png"]];
    
    [UIImagePNGRepresentation(image) writeToFile:filePath atomically:YES];
}

+ (void)removeImageWithName:(NSString *)imageName
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:[imageName stringByAppendingPathExtension:@"png"]];

    NSError *error;
    BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (!success) {
        NSLog(@"Could not delete file: %@ ",[error localizedDescription]);
    }
}

+ (UIImage *)imageWithName:(NSString *)imageName
{
    NSArray  *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir  = [documentPaths objectAtIndex:0];
    NSString  *pngFile = [documentsDir stringByAppendingPathComponent:[imageName stringByAppendingPathExtension:@"png"]];
    NSData *imageData = [NSData dataWithContentsOfFile:pngFile];
    
    return [[UIImage alloc] initWithData:imageData];
}

@end
