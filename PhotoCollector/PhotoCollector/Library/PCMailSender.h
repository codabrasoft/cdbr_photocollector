//
//  PCMailSender.h
//  PhotoCollector
//
//  Created by Igor Novik on 2015-03-19.
//  Copyright (c) 2015 CodabraSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PCMailSender : NSObject

+ (void)sendEmailWithSubject:(NSString *)subject message:(NSString *)message forRecipients:(NSArray *)recipients completion:(void(^)(NSError *error))completion;

@end
