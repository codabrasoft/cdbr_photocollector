//
//  Photo.m
//  PhotoCollector
//
//  Created by Igor Novik on 2015-03-16.
//  Copyright (c) 2015 CodabraSoft. All rights reserved.
//

#import "Photo.h"
#import "Game.h"


@implementation Photo

@dynamic name;
@dynamic game;

@end
