//
//  Player.h
//  PhotoCollector
//
//  Created by Igor Novik on 2015-03-16.
//  Copyright (c) 2015 CodabraSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Game;

@interface Player : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) Game *game;

@end
