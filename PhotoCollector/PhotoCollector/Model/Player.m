//
//  Player.m
//  PhotoCollector
//
//  Created by Igor Novik on 2015-03-16.
//  Copyright (c) 2015 CodabraSoft. All rights reserved.
//

#import "Player.h"
#import "Game.h"


@implementation Player

@dynamic name;
@dynamic email;
@dynamic phone;
@dynamic game;

@end
