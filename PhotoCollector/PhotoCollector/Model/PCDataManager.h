//
//  PCDataManager.h
//  PhotoCollector
//
//  Created by Igor Novik on 2015-03-13.
//  Copyright (c) 2015 CodabraSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Photo.h"
#import "Game.h"
#import "Player.h"

@interface PCDataManager : NSObject

@property (strong, nonatomic) Game *currentGame;

//
+ (instancetype)dataManager;

//
- (NSArray *)allGames;
- (Game *)newGameWithName:(NSString *)name;
- (void)deleteGame:(Game *)game;
- (void)deleteCurrentGame;


//
- (Photo *)createPhotoWithName:(NSString *)name;
- (void)deletePhoto:(Photo *)photo;
- (NSArray *)photosForCurrentGame;

//
- (void)addPlayerWithName:(NSString *)name email:(NSString *)email phone:(NSString *)phone;
- (void)deletePlayer:(Player *)player;
- (NSArray *)playersForCurrentGame;

@end
