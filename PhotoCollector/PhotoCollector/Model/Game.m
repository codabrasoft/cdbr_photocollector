//
//  Game.m
//  PhotoCollector
//
//  Created by Igor Novik on 2015-03-16.
//  Copyright (c) 2015 CodabraSoft. All rights reserved.
//

#import "Game.h"
#import "Photo.h"
#import "Player.h"


@implementation Game

@dynamic gameName;
@dynamic creationDate;
@dynamic photos;
@dynamic players;

@end
