//
//  PCDataManager.m
//  PhotoCollector
//
//  Created by Igor Novik on 2015-03-13.
//  Copyright (c) 2015 CodabraSoft. All rights reserved.
//

#import "PCDataManager.h"

@interface PCDataManager ()

@end

@implementation PCDataManager

+ (instancetype)dataManager
{
    static PCDataManager *sharedInstance = nil;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

#pragma mark - Game

- (NSArray *)allGames
{
    return [Game MR_findAll];
}

- (Game *)newGameWithName:(NSString *)name
{
    Game *game = [Game MR_createEntity];
    game.gameName = name;
    game.creationDate = [NSDate date];
    [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfAndWait];
    
    return game;
}

- (void)deleteGame:(Game *)game
{
    [game MR_deleteEntity];
    [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfAndWait];
}

- (void)deleteCurrentGame
{
    [self.currentGame MR_deleteEntity];
    self.currentGame = nil;
    [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfAndWait];
}

#pragma mark - Photo

- (Photo *)createPhotoWithName:(NSString *)name
{
    Photo *photo = [Photo MR_createEntity];
    photo.name = name;
    photo.game = self.currentGame;
    [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfAndWait];
    
    return photo;
}

- (NSArray *)photosForCurrentGame
{
    return [Photo MR_findByAttribute:@"game" withValue:self.currentGame];
}

- (void)deletePhoto:(Photo *)photo
{
    [photo MR_deleteEntity];
    [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfAndWait];
}

#pragma mark - Player

- (void)addPlayerWithName:(NSString *)name email:(NSString *)email phone:(NSString *)phone
{
    Player *player = [Player MR_createEntity];
    player.name = name;
    player.email = email;
    player.phone = phone;
    player.game = self.currentGame;
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfAndWait];
}

- (void)deletePlayer:(Player *)player
{
    [player MR_deleteEntity];
    [[NSManagedObjectContext MR_defaultContext] MR_saveOnlySelfAndWait];
}

- (NSArray *)playersForCurrentGame
{
    return [Player MR_findByAttribute:@"game" withValue:self.currentGame];
}

@end
