//
//  ViewController.m
//  PhotoCollector
//
//  Created by Igor Novik on 2015-03-13.
//  Copyright (c) 2015 CodabraSoft. All rights reserved.
//

#import "PCPhotoViewController.h"
#import "PCPhotoCollectionViewCell.h"
#import "PCFilesManager.h"
#import "PCAddPlayerViewController.h"
#import "PCMailSender.h"
#import "MBProgressHUD.h"

@interface PCPhotoViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDelegate, UITableViewDataSource>

//
@property (strong, nonatomic) NSMutableArray *photos;
@property (strong, nonatomic) NSMutableArray *players;

//
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;

//
- (IBAction)makePhotoButtonAction:(id)sender;
- (IBAction)backButtonAction:(id)sender;

@end

@implementation PCPhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.photos = [NSMutableArray new];
    self.players = [NSMutableArray new];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.players = [[[PCDataManager dataManager] playersForCurrentGame] mutableCopy];
    self.photos = [[[PCDataManager dataManager] photosForCurrentGame] mutableCopy];
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Actions

- (void)showAlertWithMessage:(NSString *)alertMessage
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:alertMessage message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
}

- (IBAction)makePhotoButtonAction:(id)sender
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
}

- (IBAction)sendButtonAction:(id)sender
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Email is sending...";
    self.sendButton.enabled = NO;
    
    NSArray *emails = [[PCDataManager dataManager].currentGame.players valueForKeyPath:@"@distinctUnionOfObjects.email"];
    
    __weak typeof(self) weakSelf = self;
    
    [PCMailSender sendEmailWithSubject:@"Quest Photo" message:@"" forRecipients:emails completion:^(NSError *error) {
        [MBProgressHUD hideHUDForView:weakSelf.view animated:YES];
        self.sendButton.enabled = YES;
        
        if (error) {
            [self showAlertWithMessage:@"The problem with sennding email"];
        }
    }];
}

- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[[PCDataManager dataManager] playersForCurrentGame] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PlayerCell" forIndexPath:indexPath];

    Player *player = [[[PCDataManager dataManager] playersForCurrentGame] objectAtIndex:indexPath.row];
    
    cell.textLabel.text = player.name;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        Player *player = [self.players objectAtIndex:indexPath.row];
        [[PCDataManager dataManager] deletePlayer:player];
        [self.players removeObjectAtIndex:indexPath.row];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    }
}

#pragma mark - UICollectionViewDatasource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.photos count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PCPhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"PCPhotoCollectionViewCell" forIndexPath:indexPath];
    Photo *photo = self.photos[indexPath.item];
    cell.photoImageView.image = [PCFilesManager imageWithName:photo.name];
    
    
    // cell action
    __weak typeof(self) weakSelf = self;
    cell.removePhotoBlock = ^(PCPhotoCollectionViewCell *cell) {
        [self.collectionView performBatchUpdates:^{
            // item index
            NSIndexPath *indexPath = [weakSelf.collectionView indexPathForCell:cell];
            
            // remove from database
            Photo *photo = self.photos[indexPath.item];
            [[PCDataManager dataManager] deletePhoto:photo];
            
            // remove from local array
            [self.photos removeObject:photo];
            
            // remove cell from CollectionView
            [self.collectionView deleteItemsAtIndexPaths:@[indexPath]];
            
        } completion:nil];
    };
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

//- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    Photo *photo = self.photos[indexPath.item];
//}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    NSString *UUID = [[NSUUID UUID] UUIDString];
    Photo *photo = [[PCDataManager dataManager] createPhotoWithName:UUID];
    [PCFilesManager saveImage:chosenImage withName:UUID];
    
    [self.photos addObject:photo];
    
    [self.collectionView reloadData];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"AddPlayersControllerSegue"]){
        PCAddPlayerViewController *addPlayersController = segue.destinationViewController;
        
        addPlayersController.closeActionBlock = ^(PCAddPlayerViewController *controller) {
            [controller dismissViewControllerAnimated:YES completion:nil];
        };
    }
}

@end
