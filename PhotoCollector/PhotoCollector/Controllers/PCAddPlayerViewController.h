//
//  PCAddPlayerViewController.h
//  PhotoCollector
//
//  Created by Igor Novik on 2015-03-17.
//  Copyright (c) 2015 CodabraSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PCAddPlayerViewController;

typedef void(^PCAddPlayerCloseActionBlock)(PCAddPlayerViewController *controller);

@interface PCAddPlayerViewController : UIViewController

@property (copy, nonatomic) PCAddPlayerCloseActionBlock closeActionBlock;

@end
