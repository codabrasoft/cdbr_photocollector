//
//  PCGamesListViewController.m
//  PhotoCollector
//
//  Created by Igor Novik on 2015-03-13.
//  Copyright (c) 2015 CodabraSoft. All rights reserved.
//

#import "PCGamesListViewController.h"

@interface PCGamesListViewController () <UITableViewDataSource, UITableViewDelegate>

//
@property (weak, nonatomic) IBOutlet UITableView *tableView;

//
@property (strong, nonatomic) NSMutableArray *games;

@end

@implementation PCGamesListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //
    self.games = [[[PCDataManager dataManager] allGames] mutableCopy];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.games count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GameCell" forIndexPath:indexPath];
    Game *game = self.games[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ --- %d players", game.gameName, [game.players count]];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Game *game = [self.games objectAtIndex:indexPath.row];
        [[PCDataManager dataManager] deleteGame:game];
        [self.games removeObjectAtIndex:indexPath.row];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    }
}

#pragma mark - Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"PhotoViewControllerSegue"]) {
        [PCDataManager dataManager].currentGame = [[PCDataManager dataManager] newGameWithName:[NSString stringWithFormat:@"Game %@", [Game MR_numberOfEntities]]];
    }
    else if([[segue identifier] isEqualToString:@"SelectGameSegue"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
        [PCDataManager dataManager].currentGame = [self.games objectAtIndex:indexPath.row];
    }
}

@end
