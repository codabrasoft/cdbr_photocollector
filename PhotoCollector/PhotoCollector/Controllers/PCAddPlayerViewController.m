//
//  PCAddPlayerViewController.m
//  PhotoCollector
//
//  Created by Igor Novik on 2015-03-17.
//  Copyright (c) 2015 CodabraSoft. All rights reserved.
//

#import "PCAddPlayerViewController.h"

NSInteger const kPCPhoneNumberLength = 15;

@interface PCAddPlayerViewController () <UITextFieldDelegate>

@property (strong, nonatomic) UITextField *activeTextField;

//
@property (weak, nonatomic) IBOutlet UITextField *fullNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

//
- (IBAction)closeButtonAction:(id)sender;
- (IBAction)clearButtonAction:(id)sender;
- (IBAction)addButtonAction:(id)sender;
- (void)tapAction:(UIGestureRecognizer *)gestureRecognizer;

@end

@implementation PCAddPlayerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.phoneNumberTextField.delegate = self;
    self.fullNameTextField.delegate = self;
    self.emailTextField.delegate = self;
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    [self.view addGestureRecognizer:tapGestureRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Actions

- (void)tapAction:(UIGestureRecognizer *)gestureRecognizer
{
    [self.activeTextField resignFirstResponder];
}

- (void)showAlertWithMessage:(NSString *)message
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"PhotoCollector" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
}

- (void)cleanFields
{
    self.fullNameTextField.text = @"";
    self.emailTextField.text = @"";
    self.phoneNumberTextField.text = @"";
}

- (IBAction)closeButtonAction:(id)sender
{
    if (self.closeActionBlock) {
        self.closeActionBlock(self);
    }
}

- (IBAction)clearButtonAction:(id)sender
{
    [self cleanFields];
}

- (IBAction)addButtonAction:(id)sender
{
    if ([self isFormValid])
    {
        [[PCDataManager dataManager] addPlayerWithName:self.fullNameTextField.text
                                                 email:self.emailTextField.text
                                                 phone:self.phoneNumberTextField.text];
        
        [self cleanFields];
    }
}

- (BOOL)validateEmailWithString:(NSString*)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:checkString];
}

- (BOOL)isFormValid
{
    // check empty fields
    if ([self.fullNameTextField.text isEqualToString:@""]) {
        [self showAlertWithMessage:@"Please fill the Full Name"];
        
        return NO;
    }
    if ([self.phoneNumberTextField.text isEqualToString:@""]) {
        [self showAlertWithMessage:@"Please fill the Phone"];
        
        return NO;
    }
    if ([self.emailTextField.text isEqualToString:@""]) {
        [self showAlertWithMessage:@"Please fill the Email"];
        
        return NO;
    }
    
    // validate data
    if (self.phoneNumberTextField.text.length < kPCPhoneNumberLength) {
        [self showAlertWithMessage:@"The phone number is too short"];
        return NO;
    }
    
    if (![self validateEmailWithString:self.emailTextField.text]){
        [self showAlertWithMessage:@"Your email looks incorrect"];
        return NO;
    }
    
    return YES;
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (self.phoneNumberTextField == textField && textField.text.length == 0) {
        self.phoneNumberTextField.text = @"+375(";
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (self.phoneNumberTextField == textField) {
        if (self.phoneNumberTextField.text.length == 7 && ![string isEqualToString:@""]) {
            self.phoneNumberTextField.text = [self.phoneNumberTextField.text stringByAppendingString:@")"];
        }
        else if (range.location == kPCPhoneNumberLength) {
            [self.phoneNumberTextField resignFirstResponder];
        }
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.activeTextField = textField;
    return YES;
}

@end
