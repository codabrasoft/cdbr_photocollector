//
//  PCPhotoCollectionViewCell.h
//  PhotoCollector
//
//  Created by Igor Novik on 2015-03-16.
//  Copyright (c) 2015 CodabraSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PCPhotoCollectionViewCell;

typedef void(^PCPhotoCellRemoveBlock)(PCPhotoCollectionViewCell *cell);


@interface PCPhotoCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (copy, nonatomic) PCPhotoCellRemoveBlock removePhotoBlock;

@end
