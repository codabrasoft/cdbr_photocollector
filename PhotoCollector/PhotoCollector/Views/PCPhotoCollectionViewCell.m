//
//  PCPhotoCollectionViewCell.m
//  PhotoCollector
//
//  Created by Igor Novik on 2015-03-16.
//  Copyright (c) 2015 CodabraSoft. All rights reserved.
//

#import "PCPhotoCollectionViewCell.h"

@interface PCPhotoCollectionViewCell ()

- (IBAction)removeButtonAction:(id)sender;

@end

@implementation PCPhotoCollectionViewCell

#pragma mark - Actions

- (IBAction)removeButtonAction:(id)sender
{
    if (self.removePhotoBlock != nil) {
        self.removePhotoBlock(self);
    }
}

@end
